﻿####################
IMPOSTAZIONI LIVELLI
####################
Impostazioni generali dei parametri di marcatura laser.

.. NOTE::

   |notice| La finestra :guilabel:`LIVELLI` è legata alla macchina e può differire in funzione dell'hardware installato.

************
INTRODUZIONE
************
Il file grafico normalmente contiene degli elementi che devono essere marcati.

Per collegare le aree da marcare ai parametri con cui marcare queste aree si usano dei colori predefiniti. Questi colori predefiniti sono 10 e per ognuno di questi è possibile assegnare dei parametri laser diversi.

Il laser non è una stampante a colori. I colori presenti nei livelli servono solo come riferimento visivo per associare determinati parametri a specifiche aree del disegno. Il tutto si traduce in lavorazioni diverse sul pezzo.

Questi colori vengono chiamati nel programma PRISMA "Livelli".
Ad esempio: se nel file vi è un quadrato di colore nero, il PRISMA utilizzerà i parametri impostati nel livello nero della finestra :guilabel:`LIVELLI` per marcare il quadrato.

*****************
PROPRIETÀ LIVELLI
*****************

.. _impostazioni_livelli_generale:
.. figure:: _static/impostazioni_livelli_generale.png
   :width: 14 cm
   :align: center

   Finestra *Impostazioni Livelli*

**File**

   Viene mostrato il nome della configurazione che stiamo usando, alla prima apertura si trova *default.xml*.

**Livello**

   Visualizza i colori predefiniti. Premendo sul colore si apre la finestra parametri relativa al colore.

**Icone di selezione**

   * :kbd:`Ricarica` (reimposta i valori laser salvati nel settaggio caricato per il colore in uso);
   * :kbd:`Copia da` (permette di copiare la parametrizzazione da un livello ad un altro livello);
   * :kbd:`Modalità Esperto` (vengo visualizzati dei parametri laser particolari, usati solo da utenti esperti);
   * :kbd:`Salva e Invia` (permette di salvare il set di parametri creato, assegnandogli un nome e invia i parametri impostati al laser);
   * :kbd:`Invia` (invia i parametri impostati al laser, senza salvare il set di parametri creato);
   * :kbd:`Usa layer criptato` *aggiungere descrizione*;
   * :kbd:`Livelli` (apre la cartella dei livelli pre-settati o salvati e permette di richiamare eventuali configurazioni salvate).

PARAMETRI LIVELLI
=================
Per procedere con la marcatura è necessario compilare la tabella con i valori laser desiderati definendo:

* :kbd:`Abilitazione` (se attivo il laser marcherà il colore/livello, diversamente verrà ignorato);
* :kbd:`Potenza [%]` (valore di potenza laser con cui si desidera marcare, espresso in % del valore massimo);
* :kbd:`Frequenza [kHz]` (frequenza laser (numero di spari al secondo), esempio: 20kHz = 20.000 spari/sec);
* :kbd:`Velocità [mm/s]` (velocità di spostamento laser per realizzare la marcatura);
* :kbd:`Passate` (numero di volte in cui il laser dovrà ripetere il livello selezionato);
* :kbd:`Inclinazione Pieno1 [°]` (inclinazione, espressa in gradi, delle linee che compongono la marcatura; **-1** significa campo disattivato);
* :kbd:`Inclinazione Pieno2 [°]` (possibile seconda inclinazione, espressa in gradi, delle linee che compongono la marcatura; **-1** significa campo disattivato);
* :kbd:`Pieno Bidirezionale` (preferibilmente attivo - per la spiegazione si veda capitolo relativo alla descrizione comandi) *inserire descrizione*;
* :kbd:`Spaziatura Pieno [mm]` (distanza tra una linea e l'altra con cui il laser marca l'area del livello);
* :kbd:`Ampiezza Wobble [mm]` (nelle normali lavorazioni lasciare il valore di default - per il taglio lastra o testo veloce fare riferimento ai relativi capitoli) *inserire  capitoli*
* :kbd:`Frequenza Wobble [Hz]` (vale quanto detto per :kbd:`Ampiezza Wobble [mm]`.

*********************
INVIO PARAMETRI LASER
*********************

Premere, infine, il comando :guilabel:`INVIO` per trasferire i parametri impostati al laser.


.. NOTE::

   |notice|
   Prima di eseguire la marcatura definitiva testare la reazione del materiale ai parametri impostati.
   
   Una volta definite le parametrizzazioni salvare il set parametri con un nome, così da poterlo richiamare in caso di necessità.

Proseguire l'impostazione laser seguendo le istruzioni del capitolo :numref:`MARCATURA` (:ref:`MARCATURA`).

