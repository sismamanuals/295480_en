# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020, SISMA S.p.a.
# This file is distributed under the same license as the PRISMA Software
# Manuale Utente package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PRISMA Software Manuale Utente \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-01-24 10:50+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: WIP/295480/295480/source/test_parametri.rst:3
#: efa72b6e1af44490b13b069ed385b628
msgid "TEST PARAMETRI"
msgstr ""

#: 9446fa37e79e4a59a493a1d4d3218c68
#: WIP/295480/295480/source/test_parametri.rst:4
msgid ""
"È una ricerca guidata dei parametri per la marcatura. Selezionare da "
":guilabel:`Strumenti > Test parametri`"
msgstr ""

#: WIP/295480/295480/source/test_parametri.rst:6
#: c463e5703dc1498bbfe3af17eb9051a1
msgid ""
"Questo test permette di andare ad eseguire una marcatura guidata sul "
"materiale per identificare i parametri più idonei al materiale stesso."
msgstr ""

#: 1093ab80965e41fcb0d0fcbc25cee83f
#: WIP/295480/295480/source/test_parametri.rst:8
msgid ""
"È un test rivolto ad un utente di livello base (entry level). Nel sistema"
" sono presenti altri test ma sono rivolti ad utenti più esperti che "
"conoscano già le modalità di utilizzo del programma e abbiano familiarità"
" con i parametri macchina."
msgstr ""

#: 5aed9f97828a4d139c3cd2b8e1627ef3
#: WIP/295480/295480/source/test_parametri.rst:12
msgid "RICERCA GUIDATA PARAMETRI"
msgstr ""

#: WIP/295480/295480/source/test_parametri.rst:13
#: dc5954e7443c4a0f8c215ac4dada3128
msgid "Seguire passo passo le informazioni richieste dal percorso guidato."
msgstr ""

#: 9c5c07a4eab047e58ea3992a3eb78bf9
#: WIP/295480/295480/source/test_parametri.rst:16
msgid "Focale"
msgstr ""

#: 11b0e04e5b30402aa073f531d6c5c8cc
#: WIP/295480/295480/source/test_parametri.rst:23
msgid ".. image:: _static/it/test_parametri_focale.png"
msgstr ""

#: 3bac42421d044e7bb41dbddbf5ca388a
#: WIP/295480/295480/source/test_parametri.rst:23
msgid "Finestra *Focale*"
msgstr ""

#: WIP/295480/295480/source/test_parametri.rst:27
#: fbb41eed6a0b48cea19e809f01a03b62
msgid "|warning| **Pericolo di danni a cose e persone**"
msgstr ""

#: 29547f38148f4a149a9838a7bed34586 <rst_prolog>:10
msgid ""
".. image:: _static/warning.png\n"
"   :alt: warning"
msgstr ""

#: WIP/295480/295480/source/test_parametri.rst:29
#: e56996c37da74a7d89546f9e1403018e
msgid ""
"Se si usa una focale diversa dalla focale 160 mm è opportuno indicare "
"nella fase di selezione del materiale la voce GENERICO."
msgstr ""

#: 0fc9c0fd9ac14909b48048cab717c7b3
#: WIP/295480/295480/source/test_parametri.rst:31
#: WIP/295480/295480/source/test_parametri.rst:50
#: WIP/295480/295480/source/test_parametri.rst:64
#: af5cbeb69caa49fc9e08ee89bcf38e44 bda0c80dbf2145a4983671c9fa58dd8c
msgid "Premere :kbd:`AVANTI`."
msgstr ""

#: WIP/295480/295480/source/test_parametri.rst:34
#: acb928774d0444e39d35dbe61f2b819b
msgid "Posizione"
msgstr ""

#: 1e75205ec59c413298cc6319208d26d4
#: WIP/295480/295480/source/test_parametri.rst:41
msgid ".. image:: _static/it/test_parametri_posizione.png"
msgstr ""

#: 170e441fcbe54d0c9d9b8351c13660fb
#: WIP/295480/295480/source/test_parametri.rst:41
msgid "Finestra *Posizione*"
msgstr ""

#: 50c2c63869e140749efce7c2f3c6a616
#: WIP/295480/295480/source/test_parametri.rst:43
msgid "Impostare la posizione con uno dei seguenti metodi:"
msgstr ""

#: 84f9e81784c049c5b9dbb06a0f053714
#: WIP/295480/295480/source/test_parametri.rst:45
msgid ""
"Selezionare :kbd:`HOMING` per muovere la testa nella posizione di "
"origine; *oppure*"
msgstr ""

#: 31e68f7168754de996bceda94e9d7d34
#: WIP/295480/295480/source/test_parametri.rst:46
msgid ""
"Selezionare :kbd:`INSERISCI QUOTE ATTUALI` per completare i campi "
"sottostanti con la posizione attuale; *oppure*"
msgstr ""

#: WIP/295480/295480/source/test_parametri.rst:47
#: e420ed00da8b4efe9536cea4dc93a18c
msgid "Inserire le quote manualmente dei campi :kbd:`X`, :kbd:`Y` e :kbd:`Z`."
msgstr ""

#: 456204be34a74e2696dc2d33a72d01c2
#: WIP/295480/295480/source/test_parametri.rst:49
msgid "Premere :kbd:`GO` per portare la testa nella posizione desiderata."
msgstr ""

#: 855b7c8102674efcb5631698e1658712
#: WIP/295480/295480/source/test_parametri.rst:53
msgid "Material and Process"
msgstr ""

#: 3d3c0bad82554751b2cbc7b79610d788
#: WIP/295480/295480/source/test_parametri.rst:60
msgid ".. image:: _static/it/test_parametri_material_and_process.png"
msgstr ""

#: WIP/295480/295480/source/test_parametri.rst:60
#: b8ec91a51c3b43c3b55cd7c745334d30
msgid "Finestra *Material and Process*"
msgstr ""

#: 4b1636b5c9d947c487c8acfc927d92b1
#: WIP/295480/295480/source/test_parametri.rst:62
msgid ""
"Selezionare il tipo di materiale che si intente testare. Se il materiale "
"non è presente nella lista o la focale è diversa da f=160mm si consiglia "
"di selezionare GENERICO;"
msgstr ""

#: 95029d25d8bb4a5f9fb873c5577e45cc
#: WIP/295480/295480/source/test_parametri.rst:63
msgid "Selezionare uno dei tipi di lavorazione proposta per ogni materiale;"
msgstr ""

#: WIP/295480/295480/source/test_parametri.rst:67
#: fcc6f9e56e25497b8f38a2928160d384
msgid "Test"
msgstr ""

#: 2ff95b28119a459099088d6d42a7814e
#: WIP/295480/295480/source/test_parametri.rst:74
msgid ".. image:: _static/it/test_parametri_test.png"
msgstr ""

#: 7dbd0fba1479434693aab107c52e781c
#: WIP/295480/295480/source/test_parametri.rst:74
msgid "Finestra *Test*"
msgstr ""

#: 10d2cba82513410bbad15bb8966a6861
#: WIP/295480/295480/source/test_parametri.rst:76
msgid "Nella finestra :guilabel:`TEST` si possono impostare:"
msgstr ""

#: 01a853af0e2b41d091ed5b657e0515c2
#: WIP/295480/295480/source/test_parametri.rst:78
msgid ""
":kbd:`ZOOM` (rappresenta le dimensioni massime del test, si cambia "
"attraverso il cursore);"
msgstr ""

#: 51d25bd7793f4214ad56a2b34ef6c59f
#: WIP/295480/295480/source/test_parametri.rst:79
msgid ":kbd:`STAMPA VALORI` (stampa i tempi vicino alle marcature eseguite);"
msgstr ""

#: WIP/295480/295480/source/test_parametri.rst:80
#: a7ae04000e29415d8e5ba257a485375f
msgid ""
":kbd:`MODALITÀ` (permette di scegliere se le marcature si riferiranno ad "
"un riempimento - :guilabel:`Area` - oppure ad un bordo - "
":guilabel:`Testo`);"
msgstr ""

#: 859cfd3aa303496abde5e0dae28d8379
#: WIP/295480/295480/source/test_parametri.rst:81
msgid ""
":kbd:`FORMATO` (numero di prove, a scelta tra :guilabel:`4x4`, "
":guilabel:`8x4` oppure personalizzando la matrice)"
msgstr ""

#: 24f3c8d6693347899a88a23002d1a6ef
#: WIP/295480/295480/source/test_parametri.rst:84
msgid ""
"Premere :kbd:`AVVIA ROSSO` per proiettare l'ingombro massimo all'interno "
"della camera di lavoro. Tramite il rosso è possibile posizionare il pezzo"
" in posizione desiderata."
msgstr ""

#: 116d21d182c64307bbbee97512615b83
#: WIP/295480/295480/source/test_parametri.rst:85
msgid "Chiudere la porta del laser."
msgstr ""

#: 47b18931f441476283ccde199958013c
#: WIP/295480/295480/source/test_parametri.rst:86
msgid "Premere :kbd:`AVVIA LASER` per eseguire il test."
msgstr ""

#: 131dad6bcd0b4b3a935da9309164b86f
#: WIP/295480/295480/source/test_parametri.rst:89
msgid "Avanzato"
msgstr ""

#: 11b0e04e5b30402aa073f531d6c5c8cc
#: WIP/295480/295480/source/test_parametri.rst:96
msgid ".. image:: _static/it/test_parametri_avanzato.png"
msgstr ""

#: 5e8e59e1b8144919b75f35837a9bf616
#: WIP/295480/295480/source/test_parametri.rst:96
msgid "Finestra *Avanzato*"
msgstr ""

#: 7fe9e7657b3a4647bc80ad93726c085b
#: WIP/295480/295480/source/test_parametri.rst:98
msgid "*Inserire descrizione**"
msgstr ""

#: 011b8352b27c4f97a78e2093ee476ea7
#: WIP/295480/295480/source/test_parametri.rst:102
msgid "RISULTATO TEST"
msgstr ""

#: 1d9a3bbb1e684e789e1cc2dd62dcdb89
#: WIP/295480/295480/source/test_parametri.rst:103
msgid "Se uno dei quadratini marcati soddisfa le nostre esigenze è sufficiente:"
msgstr ""

#: WIP/295480/295480/source/test_parametri.rst:105
#: d774b641b92d4aedbf344ec7398c7759
msgid ""
"Cliccare sul quadrato corrispondente (punto **1** di "
":numref:`test_parametri_test_salvataggio1`);"
msgstr ""

#: 7f555312a7b74ae187d1a90fbd7e05c4
#: WIP/295480/295480/source/test_parametri.rst:106
msgid ""
"Premere :kbd:`SALVA` (punto 2 di "
":numref:`test_parametri_test_salvataggio1`)."
msgstr ""

#: WIP/295480/295480/source/test_parametri.rst:113
#: eaa94a97708b4590ab1f158b06e27958
msgid ".. image:: _static/it/test_parametri_test_salvataggio1.png"
msgstr ""

#: 6e850973cbca428fa4d1f013054473ed
#: WIP/295480/295480/source/test_parametri.rst:113
msgid "Finestra *Test* - Salvataggio impostazioni"
msgstr ""

#: 511a5604435e44f1bb262b1f81e30c70
#: WIP/295480/295480/source/test_parametri.rst:116
msgid ""
"Si aprirà la finestra in :numref:`test_parametri_test_salvataggio2` dove "
"è necessario: #. Scegliere il file di configurazione; #. Scegliere il "
"colore del livello a cui vogliamo abbinare i parametri trovati. #. "
"Chiudere la finestra; #. Richiamare il file di configurazione modificato "
"#. Eseguire la marcatura del file con i parametri identificati."
msgstr ""

#: 38e29b16895c4287a947f1c7f922b260
#: WIP/295480/295480/source/test_parametri.rst:128
msgid ".. image:: _static/it/test_parametri_test_salvataggio2.png"
msgstr ""

#: WIP/295480/295480/source/test_parametri.rst:128
#: bc343e4761f9471190905c7b81adff4c
msgid "Finestra *Test* - Salvataggio impostazioni su livello"
msgstr ""

#: 37bb5357339a4e81aa315732fe5e9982
#: WIP/295480/295480/source/test_parametri.rst:132
msgid ""
"|notice| Se il risultato atteso è compreso tra un gruppo di parametri, è "
"possibile selezionare i quadrati relativi ai parametri scelti ed eseguire"
" un'ulteriore marcatura nel range indicato. Nella :guilabel:`LISTA TEST` "
"apparirà un nuovo test (vedere :numref:`test_parametri_test_multi`)."
msgstr ""

#: 4a57c8967e3945c29ca7a31201969a1a <rst_prolog>:14
msgid ""
".. image:: _static/notice.png\n"
"   :alt: notice"
msgstr ""

#: 86192f80fd0d467d857230c6879a0e56
#: WIP/295480/295480/source/test_parametri.rst:134
msgid ""
"Al termine del secondo test si può andare ad identificare il quadratino "
"relativo al parametro scelto e salvare il parametro scegliendo il nome "
"della configurazione Livelli e il colore a cui abbinarlo."
msgstr ""

#: 435c4e1bacd34841b56e170617fa246c
#: WIP/295480/295480/source/test_parametri.rst:141
msgid ".. image:: _static/it/test_parametri_test_multi.png"
msgstr ""

#: 5e8e59e1b8144919b75f35837a9bf616
#: WIP/295480/295480/source/test_parametri.rst:141
msgid "Finestra *Test* - Test ulteriori"
msgstr ""

#~ msgid ".. image:: _static/Immagine_it_test_parametri_focale.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_test_parametri_posizione.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_test_parametri_material_and_process.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_test_parametri_test.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_test_parametri_test_salvataggio1.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_test_parametri_test_salvataggio2.png"
#~ msgstr ""

#~ msgid ".. image:: _static/Immagine_it_test_parametri_test_multi.png"
#~ msgstr ""

