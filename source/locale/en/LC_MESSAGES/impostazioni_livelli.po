# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2020, SISMA S.p.a.
# This file is distributed under the same license as the PRISMA Software
# Manuale Utente package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PRISMA Software Manuale Utente \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-01-24 10:50+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: 60be0ebacd8e4578b1bdf4e57115357c
#: WIP/295480/295480/source/impostazioni_livelli.rst:3
msgid "IMPOSTAZIONI LIVELLI"
msgstr ""

#: 636112be63044110929c87e6513b5d2f
#: WIP/295480/295480/source/impostazioni_livelli.rst:4
msgid "Impostazioni generali dei parametri di marcatura laser."
msgstr ""

#: WIP/295480/295480/source/impostazioni_livelli.rst:8
#: d0435953b60c414b80243af5c528ddff
msgid ""
"|notice| La finestra :guilabel:`LIVELLI` è legata alla macchina e può "
"differire in funzione dell'hardware installato."
msgstr ""

#: 32a73df88f5c4d9a9868e093a7bebc06 <rst_prolog>:14
#: a74a6d5ff4c44d74bf161b3d0a324950
msgid ""
".. image:: _static/notice.png\n"
"   :alt: notice"
msgstr ""

#: WIP/295480/295480/source/impostazioni_livelli.rst:12
#: a8deac9391464f459a1ecd2a75481f9e
msgid "INTRODUZIONE"
msgstr ""

#: WIP/295480/295480/source/impostazioni_livelli.rst:13
#: f15f1f6461be46bb96bacf3435c7d0bb
msgid ""
"Il file grafico normalmente contiene degli elementi che devono essere "
"marcati."
msgstr ""

#: 75a4baa822d546488cc1e76a024d077d
#: WIP/295480/295480/source/impostazioni_livelli.rst:15
msgid ""
"Per collegare le aree da marcare ai parametri con cui marcare queste aree"
" si usano dei colori predefiniti. Questi colori predefiniti sono 10 e per"
" ognuno di questi è possibile assegnare dei parametri laser diversi."
msgstr ""

#: 941daa0dc35a4841863b0780b3a7819c
#: WIP/295480/295480/source/impostazioni_livelli.rst:17
msgid ""
"Il laser non è una stampante a colori. I colori presenti nei livelli "
"servono solo come riferimento visivo per associare determinati parametri "
"a specifiche aree del disegno. Il tutto si traduce in lavorazioni diverse"
" sul pezzo."
msgstr ""

#: 768badd1d82f465ba4c3d70c05e175c4
#: WIP/295480/295480/source/impostazioni_livelli.rst:19
msgid ""
"Questi colori vengono chiamati nel programma PRISMA \"Livelli\". Ad "
"esempio: se nel file vi è un quadrato di colore nero, il PRISMA "
"utilizzerà i parametri impostati nel livello nero della finestra "
":guilabel:`LIVELLI` per marcare il quadrato."
msgstr ""

#: WIP/295480/295480/source/impostazioni_livelli.rst:24
#: d5433feb409e4a959d08ce697f56c150
msgid "PROPRIETÀ LIVELLI"
msgstr ""

#: WIP/295480/295480/source/impostazioni_livelli.rst:31
#: fcb77adeb60d494f87a5c53986f173a6
msgid ".. image:: _static/it/impostazioni_livelli_generale.png"
msgstr ""

#: 6737409fa2b740a19f45509bc09c2e2c
#: WIP/295480/295480/source/impostazioni_livelli.rst:31
msgid "Finestra *Impostazioni Livelli*"
msgstr ""

#: 5aa19dc1f6794f75b4e8475648a087a1
#: WIP/295480/295480/source/impostazioni_livelli.rst:33
msgid "**File**"
msgstr ""

#: 0652846fea254af0bfaeda20e516a0fb
#: WIP/295480/295480/source/impostazioni_livelli.rst:35
msgid ""
"Viene mostrato il nome della configurazione che stiamo usando, alla prima"
" apertura si trova *default.xml*."
msgstr ""

#: 8142f7d31c82446490fcd52cab39a668
#: WIP/295480/295480/source/impostazioni_livelli.rst:37
msgid "**Livello**"
msgstr ""

#: 5019fd48fc4249bba81fc4d9f184919d
#: WIP/295480/295480/source/impostazioni_livelli.rst:39
msgid ""
"Visualizza i colori predefiniti. Premendo sul colore si apre la finestra "
"parametri relativa al colore."
msgstr ""

#: 31f8d339a1e549018c46e2e4b2559c15
#: WIP/295480/295480/source/impostazioni_livelli.rst:41
msgid "**Icone di selezione**"
msgstr ""

#: 182e65da01354b3382a85e138e10337c
#: WIP/295480/295480/source/impostazioni_livelli.rst:43
msgid ""
":kbd:`Ricarica` (reimposta i valori laser salvati nel settaggio caricato "
"per il colore in uso);"
msgstr ""

#: 0f0b88b7195046d0b94fcdeb029f02c0
#: WIP/295480/295480/source/impostazioni_livelli.rst:44
msgid ""
":kbd:`Copia da` (permette di copiare la parametrizzazione da un livello "
"ad un altro livello);"
msgstr ""

#: 61abcc5fc9b0461a89a9bdf25b0cc15f
#: WIP/295480/295480/source/impostazioni_livelli.rst:45
msgid ""
":kbd:`Modalità Esperto` (vengo visualizzati dei parametri laser "
"particolari, usati solo da utenti esperti);"
msgstr ""

#: WIP/295480/295480/source/impostazioni_livelli.rst:46
#: c79d3bd2b3f1425fb255e8e05c9ae5b2
msgid ""
":kbd:`Salva e Invia` (permette di salvare il set di parametri creato, "
"assegnandogli un nome e invia i parametri impostati al laser);"
msgstr ""

#: WIP/295480/295480/source/impostazioni_livelli.rst:47
#: c0adccc3e0d74c4b944c3f9155e178dc
msgid ""
":kbd:`Invia` (invia i parametri impostati al laser, senza salvare il set "
"di parametri creato);"
msgstr ""

#: 7941ea8c818940048aac4e25d253ca79
#: WIP/295480/295480/source/impostazioni_livelli.rst:48
msgid ":kbd:`Usa layer criptato` *aggiungere descrizione*;"
msgstr ""

#: 3b1bc74dee374f70b069e7a5c2809a2c
#: WIP/295480/295480/source/impostazioni_livelli.rst:49
msgid ""
":kbd:`Livelli` (apre la cartella dei livelli pre-settati o salvati e "
"permette di richiamare eventuali configurazioni salvate)."
msgstr ""

#: 61842c0f35de402884c923777c8e654b
#: WIP/295480/295480/source/impostazioni_livelli.rst:52
msgid "PARAMETRI LIVELLI"
msgstr ""

#: WIP/295480/295480/source/impostazioni_livelli.rst:53
#: dae467d77bff4fc1a7d86de0e78d5601
msgid ""
"Per procedere con la marcatura è necessario compilare la tabella con i "
"valori laser desiderati definendo:"
msgstr ""

#: 0526b6e432334613b24b3a9a0ba2e7fa
#: WIP/295480/295480/source/impostazioni_livelli.rst:55
msgid ""
":kbd:`Abilitazione` (se attivo il laser marcherà il colore/livello, "
"diversamente verrà ignorato);"
msgstr ""

#: 175238b4626a4a4883e5e6c7b540d382
#: WIP/295480/295480/source/impostazioni_livelli.rst:56
#, python-format
msgid ""
":kbd:`Potenza [%]` (valore di potenza laser con cui si desidera marcare, "
"espresso in % del valore massimo);"
msgstr ""

#: 376f7554b1f643918a4889b191361433
#: WIP/295480/295480/source/impostazioni_livelli.rst:57
msgid ""
":kbd:`Frequenza [kHz]` (frequenza laser (numero di spari al secondo), "
"esempio: 20kHz = 20.000 spari/sec);"
msgstr ""

#: WIP/295480/295480/source/impostazioni_livelli.rst:58
#: c9e5d4a920754a02a8b1aaac5e514360
msgid ""
":kbd:`Velocità [mm/s]` (velocità di spostamento laser per realizzare la "
"marcatura);"
msgstr ""

#: 9c3f372e33d14618b41c4afbdbcaef29
#: WIP/295480/295480/source/impostazioni_livelli.rst:59
msgid ""
":kbd:`Passate` (numero di volte in cui il laser dovrà ripetere il livello"
" selezionato);"
msgstr ""

#: 10bad77aee514d118a8569f902e3710f
#: WIP/295480/295480/source/impostazioni_livelli.rst:60
msgid ""
":kbd:`Inclinazione Pieno1 [°]` (inclinazione, espressa in gradi, delle "
"linee che compongono la marcatura; **-1** significa campo disattivato);"
msgstr ""

#: 916ba037089144e9846a8a0aaf63050c
#: WIP/295480/295480/source/impostazioni_livelli.rst:61
msgid ""
":kbd:`Inclinazione Pieno2 [°]` (possibile seconda inclinazione, espressa "
"in gradi, delle linee che compongono la marcatura; **-1** significa campo"
" disattivato);"
msgstr ""

#: 4b3b496cefbf46799232f11b5ed4bac5
#: WIP/295480/295480/source/impostazioni_livelli.rst:62
msgid ""
":kbd:`Pieno Bidirezionale` (preferibilmente attivo - per la spiegazione "
"si veda capitolo relativo alla descrizione comandi) *inserire "
"descrizione*;"
msgstr ""

#: WIP/295480/295480/source/impostazioni_livelli.rst:63
#: d27261a494c6482b8bc6fd5b85719fca
msgid ""
":kbd:`Spaziatura Pieno [mm]` (distanza tra una linea e l'altra con cui il"
" laser marca l'area del livello);"
msgstr ""

#: 2941abb9b94a43d99b794a7995b58501
#: WIP/295480/295480/source/impostazioni_livelli.rst:64
msgid ""
":kbd:`Ampiezza Wobble [mm]` (nelle normali lavorazioni lasciare il valore"
" di default - per il taglio lastra o testo veloce fare riferimento ai "
"relativi capitoli) *inserire  capitoli*"
msgstr ""

#: WIP/295480/295480/source/impostazioni_livelli.rst:65
#: a238d570d0f84aa9866e739559a0f599
msgid ""
":kbd:`Frequenza Wobble [Hz]` (vale quanto detto per :kbd:`Ampiezza Wobble"
" [mm]`."
msgstr ""

#: 737a6dfed3b444a7915a2e6e90934e5a
#: WIP/295480/295480/source/impostazioni_livelli.rst:69
msgid "INVIO PARAMETRI LASER"
msgstr ""

#: WIP/295480/295480/source/impostazioni_livelli.rst:71
#: d3e33bf885094aa19084cb7a7b57f322
msgid ""
"Premere, infine, il comando :guilabel:`INVIO` per trasferire i parametri "
"impostati al laser."
msgstr ""

#: 46d426a0b22b43f08f889b8da46e12c2
#: WIP/295480/295480/source/impostazioni_livelli.rst:76
msgid ""
"|notice| Prima di eseguire la marcatura definitiva testare la reazione "
"del materiale ai parametri impostati."
msgstr ""

#: 50359d635a6c4afba92c95316997360b
#: WIP/295480/295480/source/impostazioni_livelli.rst:79
msgid ""
"Una volta definite le parametrizzazioni salvare il set parametri con un "
"nome, così da poterlo richiamare in caso di necessità."
msgstr ""

#: WIP/295480/295480/source/impostazioni_livelli.rst:81
#: a076606ff8724f5bae65d08c4a8f8912
msgid ""
"Proseguire l'impostazione laser seguendo le istruzioni del capitolo "
":numref:`MARCATURA` (:ref:`MARCATURA`)."
msgstr ""

#~ msgid ".. image:: _static/Immagine_it_impostazioni_livelli.png"
#~ msgstr ""

