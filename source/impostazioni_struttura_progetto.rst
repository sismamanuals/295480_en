###############################
IMPOSTAZIONI STRUTTURA PROGETTO
###############################
Ogni elemento presente del progetto possiede delle proprietà.

Il progetto è un contenitore di file e impostazioni, che consentono di produrre la marcatura attesa. La struttura del progetto è costituita da **Piani di lavoro**.

Il piano di lavoro è un sotto-elemento che racchiude tutte le informazioni necessarie per effettuare una marcatura completa.
In questa scheda vedremo come impostare un piano di lavoro che preveda la marcatura di un file in piano.

***********************
PROPRIETÀ WORKING PLANE
***********************

.. _impostazioni_struttura_progetto:
.. figure:: _static/impostazioni_struttura_progetto.png
   :width: 10 cm
   :align: center

   Piani di lavoro


Nella finestra :guilabel:`Struttura Progetto` cliccare due volte su :kbd:`Working Plane 0` e digitare il nome del piano di lavoro (ad esempio: Piano 1) oppure lasciare il nome di default (:kbd:`Working Plane 0`).

Se si aggiungono piani di lavoro questi avranno la dicitura di default :kbd:`New Plane`.

************************
PROPRIETÀ CONFIGURAZIONE
************************

.. _impostazioni_struttura_progetto_configurazione:
.. figure:: _static/impostazioni_struttura_progetto_configurazione.png
   :width: 10 cm
   :align: center

   Finestra *CONFIGURAZIONE*

**Modalità**

   *Inserire descrizione*

**Origine**

   *Inserire descrizione*

**X [mm]**

   *Inserire descrizione*


**Y [mm]**

   *Inserire descrizione*

**Z [mm]**

   Altezza del piano da marcare. Dopo aver definito il parametro premere :kbd:`GO`. L'asse Z si porterà all'altezza impostata.

**R [°]**

   *Inserire descrizione*

**Multi**

   *Inserire descrizione*

**Inizio/Fine**

   *Inserire descrizione*

**Icone di selezione**

   * :kbd:`Pausa` (*Inserire descrizione*);
   * :kbd:`Rosso` (*Inserire descrizione*);
   * :kbd:`???` (*Inserire descrizione*);
   * :kbd:`Autofocus` (*Inserire descrizione*);
   * :kbd:`Modifica impostazioni autofocus` (*Inserire descrizione*).

***************
PROPRIETÀ FILES
***************

.. _impostazioni_struttura_progetto_files:
.. figure:: _static/impostazioni_struttura_progetto_files.png
   :width: 10 cm
   :align: center

   Finestra *FILES*


In questa finestra si può agire su alcune caratteristiche del file selezionato.

Aprendo i menù a tendina è possibile cambiare il colore di **Bordo** e **Pieno** del file, così come disattivare la visualizzazione dell'oggetto oppure bloccarne la selezione.

.. NOTE::

   |notice| Salvare il nuovo progetto creato, così da poterlo richiamare in caso di necessità.
   Per fare questo riferirsi a :numref:`salvataggio_impostazioni` (:ref:`salvataggio_impostazioni`).
