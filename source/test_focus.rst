﻿.. _test_focus:

##########
FOCUS TEST
##########
È un test da eseguirsi solo nel caso in cui si abbia in sospetto che la macchina laser abbia perso il suo punto di fuoco.

Attraverso questo test è possibile definire il nuovo punto di fuoco (o confermare l'esistente).

Per eseguirlo è sufficiente compilare ogni campo presente nella finestra ed eseguire la marcatura su una piastra di acciaio o ottone.

***************
ESECUZIONE TEST
***************
È importante sapere che il test andrà a fare una serie di marcature incrociando i range di Potenza-Frequenza-Velocità.

Per eseguire il test impostare:

* :kbd:`Altezza pezzo` (inserire l'altezza della piastrina da marcare);
* :kbd:`Altezza` (il range di altezza da campionare - attenzione che il range sia adeguato alla focale che si sta testando);
* :kbd:`Potenza` (il range di potenza da usare);
* :kbd:`Frequenza` (il range di frequenza da usare);
* :kbd:`Velocità` (il range di velocità da usare);
* :kbd:`Spaziatura riempimento` (il valore di spaziatura scelto - si consiglia di non inserire un valore troppo stringente);
* :kbd:`Linea sottile` (per utilizzare una linea sottile per la marcatura).

A questo punto si può procedere con la marcatura:

* Premere :kbd:`AVVIA ROSSO` per centrare l'area di marcatura sulla piastrina.
* Chiudere lo sportello.
* Premere :kbd:`AVVIA LASER` per eseguire la marcatura.

.. _focus_test:
.. figure:: _static/focus_test.png
   :width: 14 cm
   :align: center

   Finestra *FOCUS TEST*

**************
RISULTATO TEST
**************
Il test produrrà una serie di quadrati incisi. Alcuni quadrati potrebbero essere più incisi di altri.
La linea maggiormente incisa indica che in quel punto il laser è maggiormente a fuoco.

Per indicare al sistema che il punto di fuoco è quello che vediamo sulla piastrina è sufficiente spostare il cursore sul numero corrispondente alla linea di quadratini identificata.
Fatto questo è necessario premere su :kbd:`APPLICA` per inserire il nuovo valore trovato.

.. NOTE::

   |notice| È possibile che le linee maggiormente marcate siano 2 e siano contigue. In quella situazione è possibile selezionare anche un valore intermedio (ad esempio, tra 3 e 4).

*Se il range di altezza selezionato è molto ampio potrebbe essere necessario stringere il range intorno ad un valore identificato nel primo test focus.
Per farlo è sufficiente spostare il cursore sopra il numero della linea attorno cui vogliamo impostare il nuovo range di altezza e premere :kbd:`CONTINUA`.* *da spiegare*

A questo punto il sistema imposterà in automatico i nuovi valori dell' Altezza.
Sarà sufficiente procedere con la marcatura:

* Premere :kbd:`AVVIA ROSSO` per centrare l'area di marcatura sulla piastrina.
* Chiudere lo sportello.
* Premere :kbd:`AVVIA LASER` per eseguire la marcatura.

Una volta eseguito il secondo test si può identificare la/e linea/e di quadratini maggiormente marcati e spostare il cursore sul numero corrispondente alla linea.
Per salvare il nuovo valore di fuoco si deve premere :kbd:`APPLICA`.

A questo punto è possibile chiudere il Test Focus e rifare l'Homing della macchina prima di procedere con qualsiasi altra attività di marcatura.
