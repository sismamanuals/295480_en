.. _impostazioni_notifiche:

###########################
IMPOSTAZIONI MENÙ NOTIFICHE
###########################

.. _impostazioni_applicazione_notifiche:
.. figure:: _static/impostazioni_applicazione_notifiche.png
   :width: 14 cm
   :align: center

   Menù Notifiche


Il menù delle :guilabel:`IMPOSTAZIONI APPLICAZIONE > Notifiche` permette di impostare le modalità di spedizione notifiche.

*****************
PROPRIETÀ E-MAILS
*****************
**Abilita**

   *Inserire descrizione*

**SMTP**

   *Inserire descrizione*

**Porta**

   *Inserire descrizione*

**Da**

   *Inserire descrizione*

**A**

   *Inserire descrizione*
   Con il pulsante :kbd:`+` è possibile ... *Inserire descrizione*, con il pulsante :kbd:`-` è possibile ... *Inserire descrizione*.

**Magazzino: fine ciclo**

   *Inserire descrizione*

**Magazzino: ciclo in errore**

   *Inserire descrizione*
