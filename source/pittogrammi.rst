###########
PITTOGRAMMI
###########

Le descrizioni precedute da un pittogramma di colore rosso/giallo/arancione/azzurro contengono informazioni/prescrizioni molto importanti, anche per quanto riguarda la sicurezza, sia dell'operatore che del macchinario.
Di seguito si riportano le diciture con spiegazione di dettaglio del loro significato.

.. NOTE::
   |notice| Viene utilizzato per affrontare le pratiche non legate a lesioni fisiche.

.. WARNING::
   |warning| Indica una situazione di rischio potenziale che, se non prevista, potrebbe causare danni di minore o modesta entità.

.. CAUTION::
   |caution| Indica una situazione di rischio potenziale che, se non evitata, può causare morte o danno grave.

.. DANGER::
   |danger| Indica una situazione di rischio imminente che, se non evitata, causa morte o danno grave.
