##################
IMPOSTAZIONI LASER
##################
Impostazioni generali di marcatura laser.

.. NOTE::

   |notice| La finestra :guilabel:`LASER` è legata alla macchina e può differire in funzione dell'hardware installato.

***************
PROPRIETÀ LASER
***************

.. _impostazioni_laser_generali:
.. figure:: _static/impostazioni_laser_generali.png
   :width: 10 cm
   :align: center

   Finestra *Impostazioni Laser Generali*

**Settaggi**

   Settaggio della focale

**Livelli**

   *Inserire descrizione*

**Stato**

   Stato della macchina (funzionamento e/o allarme)

**Avanzamento**

   *Inserire descrizione*

**Tempo Lavoro**

   Tempo  dallo Start allo Stop del laser.

**Tempo Ciclo**

   Tempo impiegato per la marcatura del singolo piano.

**Tempo Laser**

   Tempo laser relativo alla singola marcatura.

I parametri sopra descritti sono di aiuto all'operatore per avere a disposizione informazioni relative alla macchina e alla marcatura, ma non sono editabili da parte dell'operatore.

*****************
PROPRIETÀ OPZIONI
*****************

.. _impostazioni_laser_opzioni:
.. figure:: _static/impostazioni_laser_opzioni.png
   :width: 10 cm
   :align: center

   Finestra *Impostazioni Laser Opzioni*


**Icone di selezione**

   * :kbd:`Bordo Normale` (marcatura di ogni singolo bordo presente nei file attivi, senza gestire la sovrapposizione degli elementi);
   * :kbd:`Pieno Normale` (marcatura di ogni singolo pieno presente nei file attivi, senza gestire la sovrapposizione degli elementi);
   * :kbd:`Bordo Non Sovrapposto` (marcatura di ogni singolo bordo presente nei file attivi, gestendo la sovrapposizione degli elementi);
   * :kbd:`Pieno Non Sovrapposto` (marcatura di ogni singolo pieno presente nei file attivi, gestendo la sovrapposizione degli elementi);
   * :kbd:`Ordine di marcatura`

      * :kbd:`Passate`
      * :kbd:`Colore`
      * :kbd:`Figure`

.. NOTE::

   |notice| Se la sovrapposizione degli elementi non è quella desiderata, agendo sulle relative icone della barra di comando dell'area di lavoro (vedi :numref:`operazioni_livelli` :ref:`operazioni_livelli`) è possibile cambiare l'ordine di sovrapposizione.

**********************
PROPRIETÀ TESTO VELOCE
**********************

.. _impostazioni_laser_testo_veloce:
.. figure:: _static/impostazioni_laser_testo_veloce.png
   :scale: 60 %
   :align: center

   Finestra *Impostazioni Laser Testo Veloce*


**Abilita**

   Sostituisce i font utilizzati nel progetto con un font monolinea, rapido da eseguire e di immediata leggibilità.

   La sostituzione avviene in fase di marcatura in modo da occupare con il nuovo carattere la posizione e la dimensione di quello sostituito.

**Carattere**

   Seleziona il font da una lista predefinita.

**Dimensione X [%]**

   Definisce la dimensione X in % dell'area occupata rispetto al rettangolo di occupazione.

**Dimensione Y [%]**

   Definisce la dimensione Y in % dell'area occupata rispetto al rettangolo di occupazione.

********************
PROPRIETÀ CENTRATURA
********************

.. _impostazioni_laser_centratura:
.. figure:: _static/impostazioni_laser_centratura.png
   :width: 10 cm
   :align: center

   Finestra *Impostazioni Laser Centratura*

Premere sulla barra di comando in alto per generare il contorno rosso dell'immagine sul piano di marcatura.

Agendo poi sui comandi della finestra :guilabel:`CENTRATURA` si deve centrare l'area di marcatura sull'area del pezzo che si desidera marcare.

I comandi che agiscono sul rosso sono (secondo l'ordine di disposizione nella finestra):

**Icone di selezione**

   * :kbd:`Abilita Tasti` (con le frecce sulla tastiera è possibile spostare la proiezione del rosso);
   * :kbd:`Applica al Progetto` (applica l'offset appena inserito con lo spostamento a tutti gli elementi attivi del piano di lavoro);
   * :kbd:`Reset` (azzera le coordinate offset correnti senza applicare le modifiche);
   * :kbd:`Imposta posizione` (forza l'invio delle coordinate quando i valori vengono inseriti manualmente nelle caselle di testo);
   * :kbd:`Blocca Posizione` (blocca ogni possibile spostamento della proiezione del rosso).

**Angolo**

   Determina l'angolo di rotazione dell'intera area di lavoro rispetto alla testa di scansione.

**X**

   Valore in mm dell'offset rispetto all'asse orizzontale.

**Y**

   Valore in mm dell'offset rispetto all'asse verticale.

**Tipo Rosso**

   Determina se la proiezione del rosso deve essere la sagoma della figura, un rettangolo oppure un cerchio (sempre con il massimo ingombro).

**Camera Live**

   * :kbd:`X` *Inserire descrizione*;
   * :kbd:`Y` *Inserire descrizione*;
   * :kbd:`S` *Inserire descrizione*;

*******************
PROPRIETÀ MARCATURA
*******************

.. _impostazioni_laser_marcatura:
.. figure:: _static/impostazioni_laser_marcatura.png
   :width: 14 cm
   :align: center

   Finestra *Impostazioni Laser Marcatura*

**Numero di Copie**

   Numero di copie che si intende marcare, se il valore è **-1** si intende copie infinite

**Copie Eseguite**

   Contatore che viene incrementato dopo ogni marcatura.

**Ciclo Continuo**

   Al termine di una operazione di marcatura ne esegue subito un'altra, senza attendere il comando :kbd:`Avvia Marcatura`. Si arresta se le copie eseguite sono uguali al numero di copie impostato.

**Azzera Contatore**

   Azzera il valore attuale di **Copie Eseguite**.

.. NOTE::

   |notice|
   A questo punto si consiglia di salvare il nuovo progetto creato, prima di proseguire con l'impostazione delle caratteristiche e dei parametri di marcatura.
   Per fare questo riferirsi a :numref:`salvataggio_impostazioni` ( :ref:`salvataggio_impostazioni` ).
